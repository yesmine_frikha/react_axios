import React from 'react';
import Crud from './component/Crud';

function App() {
  return (
    <div className="App">
      <Crud/>
    </div>
  );
}

export default App;
